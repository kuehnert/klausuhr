import React, {Component} from 'react'
import lightBlue from '@material-ui/core/colors/blue'
import orange from '@material-ui/core/colors/orange'
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles'
import ClockAppBar from "./components/clock_app_bar"
import ClockContainer from "./components/clock_container"
import './App.scss'

const theme = createMuiTheme({
    palette: {
        primary: orange,
        secondary: lightBlue
    }
})

class App extends Component {
    render() {
        return (
            <div className="App">
                <MuiThemeProvider theme={theme}>
                    <ClockAppBar/>
                    <ClockContainer/>
                </MuiThemeProvider>
            </div>
        )
    }
}

export default App
