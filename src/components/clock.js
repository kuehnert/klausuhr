import React, {Component} from 'react';
import {DateTime} from 'luxon'

export default class Clock extends Component {
    constructor(props) {
        super(props)

        this.state = {
            formattedTime: ''
        }
    }

    tick() {
        const formattedTime = DateTime
            .local()
            .toFormat(this.props.format) + " Uhr"
        this.setState({formattedTime})
    }

    componentDidMount() {
        this.tick()
        this.interval = setInterval(() => this.tick(), this.props.interval)
    }

    render() {
        const { targetTime } = this.props;

        return (
            <div className='Clock' style={{ fontSize: targetTime ? "7vw" : "11vw" }}>
                {this.state.formattedTime}
            </div>
        )
    }
}
