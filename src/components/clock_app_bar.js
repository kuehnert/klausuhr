import React from 'react'
import { IconButton, Toolbar, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';

const styles = {
  root: {
      flexGrow: 1
  },
  grow: {
      flexGrow: 1
  },
  menuButton: {
      marginLeft: -12,
      marginRight: 20
  }
};

const ClockAppBar = ({classes}) => (<AppBar position="static">
  <Toolbar>
    <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
      <img src='MSOLogo.svg' className="App-logo" alt="MSO Logo" height="48px"/>
    </IconButton>
    <Typography variant="title" className={classes.grow}>Marienschule Opladen</Typography>
    <Typography variant="title">Klausuhr</Typography>
  </Toolbar>
</AppBar>)

export default withStyles(styles)(ClockAppBar)
