import React, { Component } from 'react';
import { Settings } from 'luxon';
import Clock from './clock';
import FormatButton from './format_button';
import Timer from './timer';
import TimerForm from './timer_form';

export default class ClockContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      targetTime: null,
      interval: 1000,
      format: 'HH:mm:ss',
      label: 'Minuten'
    };

    Settings.defaultLocale = 'de';
  }

  toggleSeconds = () => {
    if (this.state.format === 'HH:mm:ss') {
      this.setState({format: 'HH:mm', label: 'Sekunden'});
    } else {
      this.setState({format: 'HH:mm:ss', label: 'Minuten'});
    }
  };

  setTimer = (newTargetTime) => {
    this.setState({targetTime: newTargetTime});
  };

  render() {
    return (
      <div>
        <div className="Clocks">
          <Clock
            format={this.state.format}
            interval={this.state.interval}
            targetTime={this.state.targetTime}/>
          <Timer
            format={this.state.format}
            interval={this.state.interval}
            targetTime={this.state.targetTime}/>
        </div>

        <FormatButton toggleSeconds={this.toggleSeconds} label={this.state.label}/>
        <TimerForm setTimer={this.setTimer} format={this.state.format}/>
      </div>
    )
  }
}
