import React, {Component} from 'react';
import Button from '@material-ui/core/Button';

class FormatButton extends Component {
  handleClick = () => {
    this.props.toggleSeconds()
  }

  render() {
    return (
      <Button onClick={this.handleClick} color='primary'> {this.props.label} </Button>
    )
  }
}

export default FormatButton
