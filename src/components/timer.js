import React, {Component} from 'react'
import { DateTime, Duration } from 'luxon'

export default class Timer extends Component {
    state = {
        targetTime: this.props.targetTime,
        formattedDuration: ''
    }

    tick() {
        const targetTime = this.props.targetTime

        if (targetTime) {
            const duration = Duration.fromMillis(targetTime - DateTime.local())
            const seconds = Math.floor(duration.as('seconds'))
            let formattedDuration = null
            if (seconds < 0) {
                // Time's over
                formattedDuration = "Abgabe"
            } else if (seconds < 60) {
                // less then 1 minute remaining
                formattedDuration = "Noch " + seconds + " Sekunden"
            } else {
                formattedDuration = duration.toFormat(this.props.format.toLowerCase())
            }

            this.setState(state => ({formattedDuration}))
        } else {
            this.setState(state => ({formattedDuration: ''}))
        }
    }

    componentDidMount() {
        this.tick()
        this.interval = setInterval(() => this.tick(), this.props.interval)
    }

    render() {
        return (
            <div>
                <div className='Clock'>
                    {this.state.formattedDuration}
                </div>
            </div>
        )
    }
}
