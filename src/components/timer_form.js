import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { DateTime, Duration } from 'luxon';
import React from 'react';
import { parseDuration } from '../util';

export default class TimerForm extends React.Component {
	state = {
		open: false,
		targetTime: null,
		targetTimeStr: '',
		durationStr: ''
	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleCancel = () => {
		this.setState({ open: false });
		this.props.setTimer(null);
	};

  handleClose = () => {
		this.setState({ open: false });
	};

	handleChange = (source) => (event) => {
		const value = event.target.value;

		if (source === 'durationStr') {
			const targetTime = parseDuration(value);

			if (targetTime) {
				const targetTimeStr = targetTime.toFormat('HH:mm:ss');
				this.setState({ durationStr: value, targetTime, targetTimeStr });
			} else {
				this.setState({ [source]: value });
			}
		} else if (source === 'targetTimeStr') {
			// Time
			let match = value.match(/^([0-2]*[0-9]):([0-5]+[0-9])$/);

			if (match) {
				let targetTime = DateTime.fromObject({
					hour: Number.parseInt(match[1], 10),
					minute: Number.parseInt(match[2], 10)
				});

				let duration = Duration.fromMillis(targetTime - DateTime.local());
				const durationStr = duration.toFormat(this.props.format.toLowerCase());

				this.setState({ durationStr, targetTime, targetTimeStr: value });
			} else {
				this.setState({ [source]: value });
			}
		}
	};

	handleSubmit = () => {
		this.setState({ open: false });
		const targetTime = parseDuration(this.state.durationStr);
		this.props.setTimer(targetTime);
	};

	render() {
		return (
			<span>
				<Button onClick={this.handleClickOpen} color="secondary">
					Timer
				</Button>
				<Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Timer</DialogTitle>
					<DialogContent>
						<DialogContentText>Gib die gewünschte Dauer (HH:MM, HH:MM:SS) oder die Zieluhrzeit an (HH:MM).</DialogContentText>
						<TextField
							id="duration"
							label="Dauer"
							placeholder="225 oder 3:45"
							autoFocus
							margin="dense"
							type="text"
							fullWidth
							onChange={this.handleChange('durationStr')}
							value={this.state.durationStr}
						/>
						<TextField
							id="targetTime"
							label="Zielzeit"
							placeholder="12:10"
							margin="dense"
							type="text"
							fullWidth
							onChange={this.handleChange('targetTimeStr')}
							value={this.state.targetTimeStr}
						/>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} color="default">
							schließen
						</Button>
						<Button onClick={this.handleCancel} color="default">
							löschen
						</Button>
						<Button onClick={this.handleSubmit} color="primary">
							stellen
						</Button>
					</DialogActions>
				</Dialog>
			</span>
		);
	}
}
