import {DateTime} from 'luxon';

export function parseDuration(durationStr) {
		let result = durationStr.match(/^\d+$/)
		if (result) {
				return DateTime
						.local()
						.plus({
								minutes: Number.parseInt(result, 10)
						});
		} else {
				result = durationStr.match(/^(\d+):([0-5][0-9]):?([0-5][0-9])?$/);

				if (result) {
						const hours = Number.parseInt(result[1], 10);
						const minutes = Number.parseInt(result[2], 10);
						const seconds = Number.parseInt(result[3], 10) || 0;

						console.log(hours, minutes, seconds);

						return DateTime
								.local()
								.plus({hours, minutes, seconds});
				}
		}

		return null;
}
